import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import Navigation from './components/sections/Navigation';
import TopBar from './components/sections/TopBar';
import Main from './components/sections/Main';
import GalleryModal from './components/sections/GalleryModal'

export const AuthContext = React.createContext()
export const GalleryModalContext = React.createContext()

const styles = {
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
  },
};

class App extends React.Component {

  state = {
    isAuth: false,
    mobileOpen: false,
    isModalOpen: false,
    collections: []
  }

  componentDidMount() {
    const storage = JSON.parse(localStorage.getItem('collections'));
    if (storage) {
      this.setState({ collections: storage })
    }
  }

  handleAddCollection = (imgModal) => {
    const { collections } = this.state;
    return () => {
      collections.push({
        src: imgModal.src,
        width: imgModal.width,
        height: imgModal.height,
        download: imgModal.download
      })
      this.setState({ collections })
      localStorage.setItem('collections', JSON.stringify(collections));
    }
  }

  handleRemoveCollection = (src) => {
    const { collections } = this.state;
    return () => {
      let newCollection = collections.filter((obj) => {
        return obj.src !== src
      })
      this.setState({ collections: newCollection })
      localStorage.setItem('collections', JSON.stringify(newCollection));
    }
  }

  handleModalToggle = (isOpen, imgModal) => {
    return () => {
      this.setState({
        isModalOpen: isOpen,
        imgModal
      })
    }
  }

  switchAuth = (isAuth) => {
    return () => {
      this.setState({ isAuth })
    }
  }

  handleDrawerToggle = () => {
    return () => {
      this.setState({ mobileOpen: !this.state.mobileOpen })
    }
  }

  handleDrawerClose = () => {
    return () => {
      this.setState({ mobileOpen: false })
    }
  }

  render() {
    const { classes } = this.props;
    const { mobileOpen, isAuth, isModalOpen, imgModal, collections } = this.state;

    return (
      <AuthContext.Provider
        value={{
          isAuth: isAuth,
          switchAuth: this.switchAuth,
          handleDrawerClose: this.handleDrawerClose
        }}>
        <GalleryModalContext.Provider
          value={{
            isModalOpen: isModalOpen,
            handleModalToggle: this.handleModalToggle,
            imgModal: imgModal,
            handleAddCollection: this.handleAddCollection,
            handleRemoveCollection: this.handleRemoveCollection,
            collections: collections
          }}>
          <BrowserRouter basename={process.env.PUBLIC_URL}>
            <div className={classes.root}>
              <TopBar mobile={mobileOpen} handleOpen={this.handleDrawerToggle} />
              <Navigation mobile={mobileOpen} handleOpen={this.handleDrawerToggle} />
              <Main />
              <GalleryModal />
            </div>
          </BrowserRouter>
        </GalleryModalContext.Provider>
      </AuthContext.Provider>
    )
  }
}

export default withStyles(styles)(App);

App.propTypes = {
  classes: PropTypes.object.isRequired
};