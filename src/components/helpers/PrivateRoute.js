import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { AuthContext } from '../../App'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthContext.Consumer>
        {
          ({ isAuth }) => (
            isAuth ? <Component {...props} /> : <Redirect to="/" />
          )
        }
      </AuthContext.Consumer>
    )}
  />
)

export default PrivateRoute