import React from 'react'
import Progress from './Progress'
import { GalleryModalContext } from '../../App'
import '../../App.css'

const Gallery = ({ photos }) => (
  <GalleryModalContext.Consumer>
    {
      ({ handleModalToggle }) => (
        <div className={`gallery ${photos.length === 0 ? 'gallery__progress' : ''}`} >
          {photos.length === 0 ?
            (
              <Progress />
            )
            :
            (
              <div className="gallery__container">
                {
                  photos.map((value, index) => (
                    <div key={index} className={`container__img ${value.width > value.height ? "horizontal" : "vertical"} ${value.width > 3000 ? "big" : ""}`}>
                      <img alt="" src={value.small} onClick={handleModalToggle(true, value)} />
                    </div>
                  ))
                }
              </div>
            )
          }
        </div>
      )
    }
  </GalleryModalContext.Consumer>
)

export default Gallery