import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import { GalleryModalContext } from '../../App'
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import SaveIcon from '@material-ui/icons/Save';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import CloseIcon from '@material-ui/icons/Close';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({

  button: {
    [theme.breakpoints.up('xs')]: {
      marginRight: "20px"
    }
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
  },
  header: {
    display: "flex",
    padding: "16px"
  },
  content: {
    padding: "16px"
  },
}));


const GalleryModal = () => {
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <GalleryModalContext.Consumer>
      {
        ({ isModalOpen, handleModalToggle, imgModal, handleAddCollection, handleRemoveCollection, collections }) => (
          isModalOpen ?
            <Dialog
              open={isModalOpen}
              scroll="body"
              fullScreen={fullScreen}
              aria-labelledby="responsive-dialog-title"
            >
              <DialogTitle className={classes.header} id="scroll-dialog-title">
                <Button variant="contained" href={imgModal.download} target="_blank" size="small" className={classes.button}>
                  <SaveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                  Save
                </Button>

                {collections.find(obj => obj.src === imgModal.src)
                  ? (
                    <Button variant="contained" size="small" className={classes.button} onClick={handleRemoveCollection(imgModal.src)}>
                      <RemoveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                      Collections
                    </Button>
                  )
                  : (
                    <Button variant="contained" size="small" className={classes.button} onClick={handleAddCollection(imgModal)}>
                      <AddIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                      Collection
                    </Button>
                  )
                }

                <Button variant="contained" size="small" onClick={handleModalToggle(false)}>
                  <CloseIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                  Close
                </Button>
              </DialogTitle>

              <DialogContent dividers className={classes.content}>
                <img alt="" src={imgModal.src} width="100%" height="auto" />
              </DialogContent>
            </Dialog> : null
        )
      }
    </GalleryModalContext.Consumer>
  );
}


export default GalleryModal