import React from 'react';
import { Switch, Route } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Search from '../../pages/Home'
import Collections from '../../pages/Collections'
import Login from '../../pages/Login'
import Random from '../../pages/Random'
import Photo from '../../pages/Photo'
import Profile from '../../pages/Profile';
import NotFound from '../../pages/NotFound'
import PrivateRoute from '../../components/helpers/PrivateRoute'


const useStyles = makeStyles(theme => ({
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
}));

const Main = () => {
  const classes = useStyles();

  return (

    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Switch>
        <Route exact path="/" component={Search} />
        <Route path="/random" component={Random} />
        <Route path="/photo" component={Photo} />
        <Route path="/collections" component={Collections} />
        <Route path="/login" component={Login} />
        <PrivateRoute path="/profile" component={Profile} />
        <Route component={NotFound} />
      </Switch>
    </main>

  )
}

export default Main
