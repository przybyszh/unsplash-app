import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { AuthContext } from '../../App.js'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Routes } from '../../utils/routes'

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  list: {
    paddingTop: "0"
  }
}));

const NavigationLinks = () => {
  const classes = useStyles();

  return (
    <AuthContext.Consumer>
      {
        ({ isAuth, switchAuth, handleDrawerClose }) => (
          < List className={classes.list}>
            {Routes.map((prop) => (
              prop.name === "Login" ?
                isAuth ?
                  <ListItem component={Link} to={"/"} button key={prop.id} onClick={switchAuth(false)}>
                    <ListItemText primary={"Logout"} />
                  </ListItem>
                  :
                  <ListItem component={Link} to={prop.path} button key={prop.id} onClick={handleDrawerClose()}>
                    <ListItemText primary={prop.name} />
                  </ListItem>
                :
                prop.isPrivate ?
                  isAuth ?
                    <ListItem component={Link} to={prop.path} button key={prop.id} onClick={handleDrawerClose()}>
                      <ListItemText primary={prop.name} />
                    </ListItem>
                    : ""
                  :
                  <ListItem component={Link} to={prop.path} button key={prop.id} onClick={handleDrawerClose()}>
                    <ListItemText primary={prop.name} />
                  </ListItem>
            ))}
          </List>
        )
      }
    </AuthContext.Consumer >
  );
};

export default withRouter(NavigationLinks);