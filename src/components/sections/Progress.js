import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Progress = () => <CircularProgress size={70} />

export default Progress