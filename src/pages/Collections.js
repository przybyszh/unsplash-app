import React from 'react';
// import Progress from '../components/sections/Progress'
import { GalleryModalContext } from '../App'
import '../App.css'

const Collections = () => (
  <GalleryModalContext.Consumer>
    {
      ({ handleModalToggle, collections }) => (
        <div className={`gallery ${collections.length === 0 ? 'gallery__progress' : ''}`}>
          {collections.length === 0 ?
            (
              "your collections is empty"
              // <Progress />
            )
            :
            (
              <div className="gallery__container">
                {
                  collections.map((value, index) => (
                    <div key={index} className={`container__img ${value.width > value.height ? "horizontal" : "vertical" && value.width > 3000 ? "big" : ""}`}>
                      <img alt="" src={value.src} onClick={handleModalToggle(true, value)} />
                    </div>
                  ))
                }
              </div>
            )
          }
        </div>
      )
    }
  </GalleryModalContext.Consumer>
)

export default Collections;