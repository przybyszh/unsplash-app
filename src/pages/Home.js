import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Gallery from "../components/sections/Gallery"
import NotFound from "./NotFound";
import { fetchAPI } from '../utils/fetches'

let timeStart = 0;
const timeOut = 500;

const styles = {
  formControl: {
    margin: "0 0 16px 0"
  }
};

class Home extends Component {

  state = {
    searchValue: "",
    page: 1,
    photos: [],
    perPage: 10,
    error: null
  }

  componentDidMount = async () => {
    window.addEventListener("scroll", this.handleScroll)
    const { page, perPage } = this.state
    const url = `${process.env.REACT_APP_UNSPLASH_URL}/photos?client_id=${process.env.REACT_APP_UNSPLASH_CLIENT_ID}&per_page=${perPage}&page=${page}`

    try {
      let fetchedPhotos = await fetchAPI(url)
      this.setState({ photos: fetchedPhotos })
    } catch (err) {
      this.setState({ error: "404 Not Found" })
    }
  }

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.handleScroll)
  }

  handleScroll = async () => {
    const { searchValue, photos, page, perPage } = this.state
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    let url = ""

    if (searchValue !== "") {
      url = `${process.env.REACT_APP_UNSPLASH_URL}/search/photos?client_id=${process.env.REACT_APP_UNSPLASH_CLIENT_ID}&per_page=${perPage}&page=${page + 1}&query=${searchValue}`
    } else {
      url = `${process.env.REACT_APP_UNSPLASH_URL}/photos?client_id=${process.env.REACT_APP_UNSPLASH_CLIENT_ID}&per_page=${perPage}&page=${page + 1}`
    }

    if (Math.round(windowHeight + window.pageYOffset) >= document.body.offsetHeight - 200) {
      try {
        let fetchedPhotos = await fetchAPI(url)
        this.setState({
          photos: [...photos, ...fetchedPhotos],
          page: page + 1
        })

      } catch (error) {
        this.setState({ error: "404 Not Found" })
      }
    }
  }

  handleSearch = (e) => {
    const value = e.target.value
    if (timeStart) clearTimeout(timeStart);
    timeStart = setTimeout(() => {
      this.searchFetch(value)
    }, timeOut);
  }

  searchFetch = (value) => {
    this.setState({ searchValue: value })
    process.nextTick(async () => {
      const { searchValue, page, perPage } = this.state
      const url = `${process.env.REACT_APP_UNSPLASH_URL}/search/photos?client_id=${process.env.REACT_APP_UNSPLASH_CLIENT_ID}&per_page=${perPage}&page=${page}&query=${searchValue}`

      if (searchValue !== "") {
        try {
          let fetchedPhotos = await fetchAPI(url)
          if (fetchedPhotos instanceof Error) {
            throw fetchAPI
          }
          this.setState({ photos: fetchedPhotos })
        } catch (err) {
          this.setState({ error: "404 Not Found" })
        }
      }
    })
  }

  render() {
    const { classes } = this.props;
    const { photos, error } = this.state

    return (
      <Fragment>
        <TextField
          className={classes.formControl}
          id="outlined-search"
          label="type to search..."
          type="search"
          fullWidth={true}
          margin="normal"
          variant="outlined"
          onChange={(e) => this.handleSearch(e)}
        />
        {
          error ?
            <NotFound /> :
            <Gallery photos={photos} />
        }
      </Fragment>
    );
  }
}

export default withStyles(styles)(Home);

Home.propTypes = {
  classes: PropTypes.object.isRequired
};