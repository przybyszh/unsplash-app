import React, { Component } from 'react'
import { withRouter, Redirect } from 'react-router-dom'
import { AuthContext } from '../App'

class Login extends Component {
  render() {
    return (
      <AuthContext.Consumer>
        {
          ({ isAuth, switchAuth }) => {
            console.log(isAuth)
            return (
              isAuth ?
                <Redirect to="/" /> : <button onClick={switchAuth(true)}>Login</button>
            )
          }
        }
      </AuthContext.Consumer>
    )
  }
}


export default withRouter(Login)
