import React from 'react'
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';


const changeLocation = ({ history }) => {
  setTimeout(() => history.replace("/"), 3000)
  return null
}

const NotFound = (props) => (
  <Container maxWidth="xs" style={{ display: 'flex', alignItems: 'center', minHeight: 'calc(100vh - 180px)' }}>
    <Grid container style={{ alignItems: 'center' }}>
      <Grid item xs={12} md={6}>
        <Typography variant="h1" align='center' color='primary' style={{ fontWeight: 'bold' }}> 404 </Typography>
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h4" align='center' style={{ textTransform: 'uppercase', fontWeight: 'bold' }}> sorry! </Typography>
        <Typography variant="subtitle1" align='center'> The page you are looking for was not found </Typography>
      </Grid>
    </Grid>
    {changeLocation(props)}
  </Container>
)

export default withRouter(NotFound)

NotFound.propTypes = {
  classes: PropTypes.object
};