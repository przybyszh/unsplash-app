import React, { Component, Fragment } from 'react';
import Gallery from "../components/sections/Gallery"
import { fetchAPI } from '../utils/fetches'

class Random extends Component {

  state = {
    photos: [],
    count: 30
  }

  componentDidMount = async () => {
    const { count } = this.state
    const url = `${process.env.REACT_APP_UNSPLASH_URL}/photos/random?client_id=${process.env.REACT_APP_UNSPLASH_CLIENT_ID}&count=${count}`
    this.setState({
      photos: await fetchAPI(url)
    })
  }

  render() {
    const { photos } = this.state

    return (
      <Fragment>
        <Gallery photos={photos} />
      </Fragment>
    );
  }
}

export default Random;