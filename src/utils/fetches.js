const pushPhotos = (result, photos) => {
  result.forEach(element => {
    return photos.push({
      src: element.urls.regular,
      small: element.urls.small,
      width: element.width,
      height: element.height,
      download: element.links.download
    })
  });
}

export const fetchAPI = async (url) => {
  let photos = []
  try {
    let res = await fetch(url)
    res = await res.json()
    if (res.total === 0) throw new Error("Not found")
    res.results ? pushPhotos(res.results, photos) : pushPhotos(res, photos)
  } catch (err) {
    return err
  }
  return photos
}