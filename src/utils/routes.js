import Home from '../pages/Home'
import Collections from '../pages/Collections'
import Random from '../pages/Random'
import Login from '../pages/Login'
import Profile from '../pages/Profile'

export const Routes = [
  {
    id: 1,
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    id: 2,
    path: '/collections',
    name: 'Collections',
    component: Collections
  },
  {
    id: 3,
    path: '/random',
    name: 'Random',
    component: Random
  },
  {
    id: 4,
    path: '/profile',
    name: 'Profile',
    component: Profile,
    isPrivate: true
  },
  {
    id: 5,
    path: '/login',
    name: 'Login',
    component: Login
  }
];
